import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.image as mpimg
from gibbs import *


def main():
	sweeps = 20

	original = mpimg.imread("original_image.bmp")
	mask = mpimg.imread("mask_image.bmp")
	dist = mpimg.imread("distorted_image.bmp")

	# get red channel
	original = original[:,:,0]
	original = np.floor((original * (1.0/255)) * 32)

	dist = dist[:,:,0]
	dist = np.floor((dist * (1.0/255)) * 32)

	# Get coordinates of holes
	holes = np.nonzero(mask)
	holes = np.transpose(holes)

	# Gibbs L1
	diff, corrected = sweep(original, dist, holes, sweeps, "Gibbs L2", gibbs, l2)
	make_plot("Gibbs L2", diff, corrected, "Sweeps")

	# PDE
	# diff, corrected = sweep(original, dist, holes, sweeps, "PDE", pde)
	# make_plot("PDE", diff, corrected, "Iterations")

	return diff, corrected


if __name__ == "__main__":
	diff, corrected = main()