# Gibbs sampler 

import numpy as np
from bisect import bisect_left
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import gridspec


def gibbs(img, x, y, sweep, L):
	pdf = {}
	# beta = 1.0
	# small number means higher temperature, i.e., faster change
	# as t increases, temperature increases
	beta = (sweep + 1) * 0.02
	
	up = img[x, y+1]
	down = img[x, y-1]
	left = img[x-1, y]
	right = img[x+1, y]

	for intensity in range(0,32):
		gradient = (L(intensity - right) + L(intensity - left)
				+ L(intensity - up) + L(intensity - down))
		pdf[intensity] = np.exp(-beta * gradient)

	pdf_array = np.array([pdf[i] for i in sorted(pdf.keys())])
	Z = np.sum(pdf_array) # scaling factor

	pdf_array = pdf_array * (1.0/Z)  # normalize the array

	cdf = np.cumsum(pdf_array)

	u = np.random.rand()
	updated_intensity = bisect_left(cdf, u)

	return updated_intensity


def pde(img, x, y):
	eta = 0.4

	this = img[x,y]
	up = img[x, y+1]
	down = img[x, y-1]
	left = img[x-1, y]
	right = img[x+1, y]

	return this + eta * (up + down + left + right - (4 * this))


def l1(val): 
	return val**2


def l2(val):
	return np.abs(val)


def sweep(original, dist, holes, sweeps, alg_name, func, *args):
	diff = []
	working_img = np.copy(dist)	

	diff.append(get_error(original, working_img))
	
	print "\nBeginning %s!" % alg_name

	for sweep in range(0, sweeps):
		print "Running sweep %d of %d..." % (sweep + 1, sweeps)
		update_values(working_img, holes, func, sweep, *args)
		diff.append(get_error(original, working_img))

	return diff, working_img


def update_values(dist_img, holes, func, sweep, *args):
	for x,y in holes: 
		if func == gibbs:
			dist_img[x,y] = func(dist_img, x, y, sweep, *args)
		else: 
			dist_img[x,y] = func(dist_img, x, y, *args)


def get_error(original, corrected):
	# append to error array after each sweep/iteration
	return (1.0/original.size) * np.sum((corrected - original)**2)


def make_plot(plot_name, diff, corr_img, iter_name):
	fig = plt.figure(1, figsize=(16, 16))
	gs = gridspec.GridSpec(1, 2, height_ratios=[1,1], width_ratios=[1,1]) 
	fig.add_subplot(gs[0])
	plt.plot(diff, marker='o')
	plt.xlabel(iter_name)
	plt.ylabel("Per-pixel error")
	plt.title("Error vs. %s" % iter_name)

	fig.add_subplot(gs[1])
	plt.imshow(np.flipud(corr_img), cmap=cm.Greys_r)
	plt.title("Corrected Image")
	
	plt.suptitle(plot_name)
	plt.savefig('%s_1_0.png' % plot_name, bbox_inches='tight')
	
	plt.show()





